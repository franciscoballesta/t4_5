/**
 * 
 */

/**
 * @author francis
 *
 */
public class T4_5 {

	/**
	- Programa java que declare cuatro variables enteras A, B, C y D y
	asígnale un valor a cada una. A continuación realiza las instrucciones
	necesarias para que:
	• B tome el valor de C
	• C tome el valor de A
	• A tome el valor de D
	• D tome el valor de B
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int A=1, B=2, C=3, D=4;
		B=C;
		C=A;
		A=D;
		D=B;
	}

}
